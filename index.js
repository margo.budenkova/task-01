const express = require('express')
const app = express()
const bodyParser = require('body-parser');
app.use(bodyParser.text());


  app.post('/square', (req, res) => {
    const number = parseFloat(req.body);

    console.log(req.body)
    const square = number * number;

    const response = {
      number: number,
      square: square
    };

    console.log(response)

    res.json(response);

  });

  app.post('/reverse', (req, res) => {
    const string =req.body
    console.log(string)

    const response = string.split('').reverse().join(''); 
    console.log(response); 

    res.json(response)
  })

  app.get('/date/:year/:month/:day', (req,res) => {
    const year = req.params.year; 
    const month = req.params.month - 1; 
    const day = req.params.day;

    console.log(year, month, day)

    const date = new Date(year, month, day);

    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekday = weekDays[date.getDay()];
    const isLeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);


    const today = new Date();
    let difference = Math.ceil((date.getTime() - today.getTime()) / (1000 * 3600 * 24));

    if (difference < 0) difference *= -1;
    

    const response = {
      "weekDay": weekday,
      "isLeapYear": isLeapYear, 
      "difference": difference

    }

    res.json(response);
  })



const port = process.env.PORT || 56201;
app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})